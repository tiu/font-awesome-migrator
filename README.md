# FontAwesomeMigrator

Migrates Font Awesome 4 Icon Helper and Classes to Font Awesome 5

## Installation

Add this line to your application's Gemfile until you are done migrating:

```ruby
group :development do
  gem 'font-awesome-migrator', git: 'https://bitbucket.org/tiu/font-awesome-migrator'
end
```

Upgrade your 'font-awesome-sass' gem from 4 to 5.
See https://github.com/FortAwesome/font-awesome-sass for more details

```ruby
  gem 'font-awesome-sass', '~> 5.0'
```

And then execute:

    $ bundle

Migrate your files:

## Usage

```
  migrate_font_awesome app/views/test/index.html.erb  # migrate a single file
  migrate_font_awesome app/views/**/*.erb  # migrate just view templates
  migrate_font_awesome                     # migrate entire project
```

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/tiu/font-awesome-migrator

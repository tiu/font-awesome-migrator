require "font-awesome-migrator/version"
require 'set'

module FontAwesomeMigrator
  extend self
  # Upgrading Fontawesome 4 to 5
  # https://fontawesome.com/how-to-use/upgrading-from-4#upgrade-steps

  # Regex will match icon helper with and without parentheses
  ICON_HELPER_REGEX = /(?<=icon.)(('|")(?<icon_name>[a-z\-\d]+)('|"))/
  ICON_CLASS_REGEX = /(fa fa-(?<icon_name>[a-z0-9\-]*))/

  # Icons
  # https://fontawesome.com/icons?d=gallery&m=free
  # to add another replacement: { old_class: 'old-icon-name-o' => { new_class: 'new-icon-name', prefix: 'far|fas|fab' }
  ICONS = {
    'address-book-o' => { new_class: 'address-book', prefix: 'far' },
    'address-card-o' => { new_class: 'address-card', prefix: 'far' },
    'area-chart' => { new_class: 'chart-area', prefix: 'fas' },
    'arrow-circle-o-down' => { new_class: 'arrow-alt-circle-down', prefix: 'far' },
    'arrow-circle-o-left' => { new_class: 'arrow-alt-circle-left', prefix: 'far' },
    'arrow-circle-o-right' => { new_class: 'arrow-alt-circle-right', prefix: 'far' },
    'arrow-circle-o-up' => { new_class: 'arrow-alt-circle-up', prefix: 'far' },
    'arrows-alt' => { new_class: 'expand-arrows-alt', prefix: 'fas' },
    'arrows-h' => { new_class: 'arrows-alt-h', prefix: 'fas' },
    'arrows-v' => { new_class: 'arrows-alt-v', prefix: 'fas' },
    'arrows' => { new_class: 'arrows-alt', prefix: 'fas' },
    'asl-interpreting' => { new_class: 'american-sign-language-interpreting', prefix: 'fas' },
    'automobile' => { new_class: 'car', prefix: 'fas' },
    'bank' => { new_class: 'university', prefix: 'fas' },
    'bar-chart-o' => { new_class: 'chart-bar', prefix: 'far' },
    'bar-chart' => { new_class: 'chart-bar', prefix: 'far' },
    'bathtub' => { new_class: 'bath', prefix: 'fas' },
    'battery-0' => { new_class: 'battery-empty', prefix: 'fas' },
    'battery-1' => { new_class: 'battery-quarter', prefix: 'fas' },
    'battery-2' => { new_class: 'battery-half', prefix: 'fas' },
    'battery-3' => { new_class: 'battery-three-quarters', prefix: 'fas' },
    'battery-4' => { new_class: 'battery-full', prefix: 'fas' },
    'battery' => { new_class: 'battery-full', prefix: 'fas' },
    'bell-o' => { new_class: 'bell', prefix: 'far' },
    'bell-slash-o' => { new_class: 'bell-slash', prefix: 'far' },
    'bitbucket-square' => { new_class: 'bitbucket', prefix: 'fab' },
    'bitcoin' => { new_class: 'btc', prefix: 'fab' },
    'bookmark-o' => { new_class: 'bookmark', prefix: 'far' },
    'building-o' => { new_class: 'building', prefix: 'far' },
    'cab' => { new_class: 'taxi', prefix: 'fas' },
    'calendar-check-o' => { new_class: 'calendar-check', prefix: 'far' },
    'calendar-minus-o' => { new_class: 'calendar-minus', prefix: 'far' },
    'calendar-o' => { new_class: 'calendar', prefix: 'far' },
    'calendar-plus-o' => { new_class: 'calendar-plus', prefix: 'far' },
    'calendar-times-o' => { new_class: 'calendar-times', prefix: 'far' },
    'calendar' => { new_class: 'calendar-alt', prefix: 'fas' },
    'caret-square-o-down' => { new_class: 'caret-square-down', prefix: 'far' },
    'caret-square-o-left' => { new_class: 'caret-square-left', prefix: 'far' },
    'caret-square-o-right' => { new_class: 'caret-square-right', prefix: 'far' },
    'caret-square-o-up' => { new_class: 'caret-square-up', prefix: 'far' },
    'cc' => { new_class: 'closed-captioning', prefix: 'far' },
    'chain-broken' => { new_class: 'unlink', prefix: 'fas' },
    'chain' => { new_class: 'link', prefix: 'fas' },
    'check-circle-o' => { new_class: 'check-circle', prefix: 'far' },
    'check-square-o' => { new_class: 'check-square', prefix: 'far' },
    'circle-o-notch' => { new_class: 'circle-notch', prefix: 'fas' },
    'circle-o' => { new_class: 'circle', prefix: 'far' },
    'circle-thin' => { new_class: 'circle', prefix: 'far' },
    'clock-o' => { new_class: 'clock', prefix: 'far' },
    'close' => { new_class: 'times', prefix: 'fas' },
    'cloud-download' => { new_class: 'cloud-download-alt', prefix: 'fas' },
    'cloud-upload' => { new_class: 'cloud-upload-alt', prefix: 'fas' },
    'cny' => { new_class: 'yen-sign', prefix: 'fas' },
    'code-fork' => { new_class: 'code-branch', prefix: 'fas' },
    'comment-o' => { new_class: 'comment', prefix: 'far' },
    'commenting-o' => { new_class: 'comment-alt', prefix: 'far' },
    'commenting' => { new_class: 'comment-alt', prefix: 'fas' },
    'comments-o' => { new_class: 'comments', prefix: 'far' },
    'credit-card-alt' => { new_class: 'credit-card', prefix: 'fas' },
    'cutlery' => { new_class: 'utensils', prefix: 'fas' },
    'dashboard' => { new_class: 'tachometer-alt', prefix: 'fas' },
    'deafness' => { new_class: 'deaf', prefix: 'fas' },
    'dedent' => { new_class: 'outdent', prefix: 'fas' },
    'diamond' => { new_class: 'gem', prefix: 'far' },
    'dollar' => { new_class: 'dollar-sign', prefix: 'fas' },
    'dot-circle-o' => { new_class: 'dot-circle', prefix: 'far' },
    'drivers-license-o' => { new_class: 'id-card', prefix: 'far' },
    'drivers-license' => { new_class: 'id-card', prefix: 'fas' },
    'eercast' => { new_class: 'sellcast', prefix: 'fab' },
    'envelope-o' => { new_class: 'envelope', prefix: 'far' },
    'envelope-open-o' => { new_class: 'envelope-open', prefix: 'far' },
    'eur' => { new_class: 'euro-sign', prefix: 'fas' },
    'euro' => { new_class: 'euro-sign', prefix: 'fas' },
    'exchange' => { new_class: 'exchange-alt', prefix: 'fas' },
    'external-link-square' => { new_class: 'external-link-square-alt', prefix: 'fas' },
    'external-link' => { new_class: 'external-link-alt', prefix: 'fas' },
    'eyedropper' => { new_class: 'eye-dropper', prefix: 'fas' },
    'fa' => { new_class: 'font-awesome', prefix: 'fab' },
    'facebook-f' => { new_class: 'facebook-f', prefix: 'fab' },
    'facebook-official' => { new_class: 'facebook', prefix: 'fab' },
    'facebook' => { new_class: 'facebook-f', prefix: 'fab' },
    'feed' => { new_class: 'rss', prefix: 'fas' },
    'file-archive-o' => { new_class: 'file-archive', prefix: 'far' },
    'file-audio-o' => { new_class: 'file-audio', prefix: 'far' },
    'file-code-o' => { new_class: 'file-code', prefix: 'far' },
    'file-excel-o' => { new_class: 'file-excel', prefix: 'far' },
    'file-image-o' => { new_class: 'file-image', prefix: 'far' },
    'file-movie-o' => { new_class: 'file-video', prefix: 'far' },
    'file-o' => { new_class: 'file', prefix: 'far' },
    'file-pdf-o' => { new_class: 'file-pdf', prefix: 'far' },
    'file-photo-o' => { new_class: 'file-image', prefix: 'far' },
    'file-picture-o' => { new_class: 'file-image', prefix: 'far' },
    'file-powerpoint-o' => { new_class: 'file-powerpoint', prefix: 'far' },
    'file-sound-o' => { new_class: 'file-audio', prefix: 'far' },
    'file-text-o' => { new_class: 'file-alt', prefix: 'far' },
    'file-text' => { new_class: 'file-alt', prefix: 'fas' },
    'file-video-o' => { new_class: 'file-video', prefix: 'far' },
    'file-word-o' => { new_class: 'file-word', prefix: 'far' },
    'file-zip-o' => { new_class: 'file-archive', prefix: 'far' },
    'files-o' => { new_class: 'copy', prefix: 'far' },
    'flag-o' => { new_class: 'flag', prefix: 'far' },
    'flash' => { new_class: 'bolt', prefix: 'fas' },
    'floppy-o' => { new_class: 'save', prefix: 'far' },
    'folder-o' => { new_class: 'folder', prefix: 'far' },
    'folder-open-o' => { new_class: 'folder-open', prefix: 'far' },
    'frown-o' => { new_class: 'frown', prefix: 'far' },
    'futbol-o' => { new_class: 'futbol', prefix: 'far' },
    'gbp' => { new_class: 'pound-sign', prefix: 'fas' },
    'ge' => { new_class: 'empire', prefix: 'fab' },
    'gear' => { new_class: 'cog', prefix: 'fas' },
    'gears' => { new_class: 'cogs', prefix: 'fas' },
    'gittip' => { new_class: 'gratipay', prefix: 'fab' },
    'glass' => { new_class: 'glass-martini', prefix: 'fas' },
    'google-plus-circle' => { new_class: 'google-plus', prefix: 'fab' },
    'google-plus-official' => { new_class: 'google-plus', prefix: 'fab' },
    'google-plus' => { new_class: 'google-plus-g', prefix: 'fab' },
    'group' => { new_class: 'users', prefix: 'fas' },
    'hand-grab-o' => { new_class: 'hand-rock', prefix: 'far' },
    'hand-lizard-o' => { new_class: 'hand-lizard', prefix: 'far' },
    'hand-o-down' => { new_class: 'hand-point-down', prefix: 'far' },
    'hand-o-left' => { new_class: 'hand-point-left', prefix: 'far' },
    'hand-o-right' => { new_class: 'hand-point-right', prefix: 'far' },
    'hand-o-up' => { new_class: 'hand-point-up', prefix: 'far' },
    'hand-paper-o' => { new_class: 'hand-paper', prefix: 'far' },
    'hand-peace-o' => { new_class: 'hand-peace', prefix: 'far' },
    'hand-pointer-o' => { new_class: 'hand-pointer', prefix: 'far' },
    'hand-rock-o' => { new_class: 'hand-rock', prefix: 'far' },
    'hand-scissors-o' => { new_class: 'hand-scissors', prefix: 'far' },
    'hand-spock-o' => { new_class: 'hand-spock', prefix: 'far' },
    'hand-stop-o' => { new_class: 'hand-paper', prefix: 'far' },
    'handshake-o' => { new_class: 'handshake', prefix: 'far' },
    'hard-of-hearing' => { new_class: 'deaf', prefix: 'fas' },
    'hdd-o' => { new_class: 'hdd', prefix: 'far' },
    'header' => { new_class: 'heading', prefix: 'fas' },
    'heart-o' => { new_class: 'heart', prefix: 'far' },
    'hospital-o' => { new_class: 'hospital', prefix: 'far' },
    'hotel' => { new_class: 'bed', prefix: 'fas' },
    'hourglass-1' => { new_class: 'hourglass-start', prefix: 'fas' },
    'hourglass-2' => { new_class: 'hourglass-half', prefix: 'fas' },
    'hourglass-3' => { new_class: 'hourglass-end', prefix: 'fas' },
    'hourglass-o' => { new_class: 'hourglass', prefix: 'far' },
    'id-card-o' => { new_class: 'id-card', prefix: 'far' },
    'ils' => { new_class: 'shekel-sign', prefix: 'fas' },
    'image' => { new_class: 'image', prefix: 'far' },
    'inr' => { new_class: 'rupee-sign', prefix: 'fas' },
    'institution' => { new_class: 'university', prefix: 'fas' },
    'intersex' => { new_class: 'transgender', prefix: 'fas' },
    'jpy' => { new_class: 'yen-sign', prefix: 'fas' },
    'keyboard-o' => { new_class: 'keyboard', prefix: 'far' },
    'krw' => { new_class: 'won-sign', prefix: 'fas' },
    'legal' => { new_class: 'gavel', prefix: 'fas' },
    'lemon-o' => { new_class: 'lemon', prefix: 'far' },
    'level-down' => { new_class: 'level-down-alt', prefix: 'fas' },
    'level-up' => { new_class: 'level-up-alt', prefix: 'fas' },
    'life-bouy' => { new_class: 'life-ring', prefix: 'far' },
    'life-buoy' => { new_class: 'life-ring', prefix: 'far' },
    'life-saver' => { new_class: 'life-ring', prefix: 'far' },
    'lightbulb-o' => { new_class: 'lightbulb', prefix: 'far' },
    'line-chart' => { new_class: 'chart-line', prefix: 'fas' },
    'linkedin-square' => { new_class: 'linkedin', prefix: 'fab' },
    'linkedin' => { new_class: 'linkedin-in', prefix: 'fab' },
    'long-arrow-down' => { new_class: 'long-arrow-alt-down', prefix: 'fas' },
    'long-arrow-left' => { new_class: 'long-arrow-alt-left', prefix: 'fas' },
    'long-arrow-right' => { new_class: 'long-arrow-alt-right', prefix: 'fas' },
    'long-arrow-up' => { new_class: 'long-arrow-alt-up', prefix: 'fas' },
    'mail-forward' => { new_class: 'share', prefix: 'fas' },
    'mail-reply-all' => { new_class: 'reply-all', prefix: 'fas' },
    'mail-reply' => { new_class: 'reply', prefix: 'fas' },
    'map-marker' => { new_class: 'map-marker-alt', prefix: 'fas' },
    'map-o' => { new_class: 'map', prefix: 'far' },
    'meanpath' => { new_class: 'font-awesome', prefix: 'fab' },
    'meh-o' => { new_class: 'meh', prefix: 'far' },
    'minus-square-o' => { new_class: 'minus-square', prefix: 'far' },
    'mobile-phone' => { new_class: 'mobile-alt', prefix: 'fas' },
    'mobile' => { new_class: 'mobile-alt', prefix: 'fas' },
    'money' => { new_class: 'money-bill-alt', prefix: 'far' },
    'moon-o' => { new_class: 'moon', prefix: 'far' },
    'mortar-board' => { new_class: 'graduation-cap', prefix: 'fas' },
    'navicon' => { new_class: 'bars', prefix: 'fas' },
    'newspaper-o' => { new_class: 'newspaper', prefix: 'far' },
    'paper-plane-o' => { new_class: 'paper-plane', prefix: 'far' },
    'paste' => { new_class: 'clipboard', prefix: 'far' },
    'pause-circle-o' => { new_class: 'pause-circle', prefix: 'far' },
    'pencil-square-o' => { new_class: 'edit', prefix: 'far' },
    'pencil-square' => { new_class: 'pen-square', prefix: 'fas' },
    'pencil' => { new_class: 'pencil-alt', prefix: 'fas' },
    'photo' => { new_class: 'image', prefix: 'far' },
    'picture-o' => { new_class: 'image', prefix: 'far' },
    'pie-chart' => { new_class: 'chart-pie', prefix: 'fas' },
    'play-circle-o' => { new_class: 'play-circle', prefix: 'far' },
    'plus-square-o' => { new_class: 'plus-square', prefix: 'far' },
    'question-circle-o' => { new_class: 'question-circle', prefix: 'far' },
    'ra' => { new_class: 'rebel', prefix: 'fab' },
    'refresh' => { new_class: 'sync', prefix: 'fas' },
    'remove' => { new_class: 'times', prefix: 'fas' },
    'reorder' => { new_class: 'bars', prefix: 'fas' },
    'repeat' => { new_class: 'redo', prefix: 'fas' },
    'resistance' => { new_class: 'rebel', prefix: 'fab' },
    'rmb' => { new_class: 'yen-sign', prefix: 'fas' },
    'rotate-left' => { new_class: 'undo', prefix: 'fas' },
    'rotate-right' => { new_class: 'redo', prefix: 'fas' },
    'rouble' => { new_class: 'ruble-sign', prefix: 'fas' },
    'rub' => { new_class: 'ruble-sign', prefix: 'fas' },
    'ruble' => { new_class: 'ruble-sign', prefix: 'fas' },
    'rupee' => { new_class: 'rupee-sign', prefix: 'fas' },
    's15' => { new_class: 'bath', prefix: 'fas' },
    'scissors' => { new_class: 'cut', prefix: 'fas' },
    'send-o' => { new_class: 'paper-plane', prefix: 'far' },
    'send' => { new_class: 'paper-plane', prefix: 'fas' },
    'share-square-o' => { new_class: 'share-square', prefix: 'far' },
    'shekel' => { new_class: 'shekel-sign', prefix: 'fas' },
    'sheqel' => { new_class: 'shekel-sign', prefix: 'fas' },
    'shield' => { new_class: 'shield-alt', prefix: 'fas' },
    'sign-in' => { new_class: 'sign-in-alt', prefix: 'fas' },
    'sign-out' => { new_class: 'sign-out-alt', prefix: 'fas' },
    'signing' => { new_class: 'sign-language', prefix: 'fas' },
    'sliders' => { new_class: 'sliders-h', prefix: 'fas' },
    'smile-o' => { new_class: 'smile', prefix: 'far' },
    'snowflake-o' => { new_class: 'snowflake', prefix: 'far' },
    'soccer-ball-o' => { new_class: 'futbol', prefix: 'far' },
    'sort-alpha-asc' => { new_class: 'sort-alpha-down', prefix: 'fas' },
    'sort-alpha-desc' => { new_class: 'sort-alpha-up', prefix: 'fas' },
    'sort-amount-asc' => { new_class: 'sort-amount-down', prefix: 'fas' },
    'sort-amount-desc' => { new_class: 'sort-amount-up', prefix: 'fas' },
    'sort-asc' => { new_class: 'sort-up', prefix: 'fas' },
    'sort-desc' => { new_class: 'sort-down', prefix: 'fas' },
    'sort-numeric-asc' => { new_class: 'sort-numeric-down', prefix: 'fas' },
    'sort-numeric-desc' => { new_class: 'sort-numeric-up', prefix: 'fas' },
    'spoon' => { new_class: 'utensil-spoon', prefix: 'fas' },
    'square-o' => { new_class: 'square', prefix: 'far' },
    'star-half-empty' => { new_class: 'star-half', prefix: 'far' },
    'star-half-full' => { new_class: 'star-half', prefix: 'far' },
    'star-half-o' => { new_class: 'star-half', prefix: 'far' },
    'star-o' => { new_class: 'star', prefix: 'far' },
    'sticky-note-o' => { new_class: 'sticky-note', prefix: 'far' },
    'stop-circle-o' => { new_class: 'stop-circle', prefix: 'far' },
    'sun-o' => { new_class: 'sun', prefix: 'far' },
    'support' => { new_class: 'life-ring', prefix: 'far' },
    'tablet' => { new_class: 'tablet-alt', prefix: 'fas' },
    'tachometer' => { new_class: 'tachometer-alt', prefix: 'fas' },
    'television' => { new_class: 'tv', prefix: 'fas' },
    'thermometer-0' => { new_class: 'thermometer-empty', prefix: 'fas' },
    'thermometer-1' => { new_class: 'thermometer-quarter', prefix: 'fas' },
    'thermometer-2' => { new_class: 'thermometer-half', prefix: 'fas' },
    'thermometer-3' => { new_class: 'thermometer-three-quarters', prefix: 'fas' },
    'thermometer-4' => { new_class: 'thermometer-full', prefix: 'fas' },
    'thermometer' => { new_class: 'thermometer-full', prefix: 'fas' },
    'thumb-tack' => { new_class: 'thumbtack', prefix: 'fas' },
    'thumbs-o-down' => { new_class: 'thumbs-down', prefix: 'far' },
    'thumbs-o-up' => { new_class: 'thumbs-up', prefix: 'far' },
    'ticket' => { new_class: 'ticket-alt', prefix: 'fas' },
    'times-circle-o' => { new_class: 'times-circle', prefix: 'far' },
    'times-rectangle-o' => { new_class: 'window-close', prefix: 'far' },
    'times-rectangle' => { new_class: 'window-close', prefix: 'fas' },
    'toggle-down' => { new_class: 'caret-square-down', prefix: 'far' },
    'toggle-left' => { new_class: 'caret-square-left', prefix: 'far' },
    'toggle-right' => { new_class: 'caret-square-right', prefix: 'far' },
    'toggle-up' => { new_class: 'caret-square-up', prefix: 'far' },
    'trash-o' => { new_class: 'trash-alt', prefix: 'far' },
    'trash' => { new_class: 'trash-alt', prefix: 'fas' },
    'try' => { new_class: 'lira-sign', prefix: 'fas' },
    'turkish-lira' => { new_class: 'lira-sign', prefix: 'fas' },
    'unsorted' => { new_class: 'sort', prefix: 'fas' },
    'usd' => { new_class: 'dollar-sign', prefix: 'fas' },
    'user-circle-o' => { new_class: 'user-circle', prefix: 'far' },
    'user-o' => { new_class: 'user', prefix: 'far' },
    'vcard-o' => { new_class: 'address-card', prefix: 'far' },
    'vcard' => { new_class: 'address-card', prefix: 'fas' },
    'video-camera' => { new_class: 'video', prefix: 'fas' },
    'vimeo' => { new_class: 'vimeo-v', prefix: 'fab' },
    'volume-control-phone' => { new_class: 'phone-volume', prefix: 'fas' },
    'warning' => { new_class: 'exclamation-triangle', prefix: 'fas' },
    'wechat' => { new_class: 'weixin', prefix: 'fab' },
    'wheelchair-alt' => { new_class: 'accessible-icon', prefix: 'fab' },
    'window-close-o' => { new_class: 'window-close', prefix: 'far' },
    'won' => { new_class: 'won-sign', prefix: 'fas' },
    'y-combinator-square' => { new_class: 'hacker-news', prefix: 'fab' },
    'yc-square' => { new_class: 'hacker-news', prefix: 'fab' },
    'yc' => { new_class: 'y-combinator', prefix: 'fab' },
    'yen' => { new_class: 'yen-sign', prefix: 'fas' },
    'youtube-play' => { new_class: 'youtube', prefix: 'fab' },
    'youtube-square' => { new_class: 'youtube', prefix: 'fab' }
  }.freeze

  BRANDS = %w[500px adn amazon android angellist apple bandcamp behance behance-square bitbucket bitbucket-square bitcoin (alias) black-tie bluetooth bluetooth-b btc buysellads cc-amex cc-diners-club cc-discover cc-jcb cc-mastercard cc-paypal cc-stripe cc-visa chrome codepen codiepie connectdevelop contao css3 dashcube delicious deviantart digg dribbble dropbox drupal edge eercast empire envira etsy expeditedssl fa (alias) facebook facebook-f (alias) facebook-official facebook-square firefox first-order flickr font-awesome fonticons fort-awesome forumbee foursquare free-code-camp ge (alias) get-pocket gg gg-circle git git-square github github-alt github-square gitlab gittip (alias) glide glide-g google google-plus google-plus-circle (alias) google-plus-official google-plus-square google-wallet gratipay grav hacker-news houzz html5 imdb instagram internet-explorer ioxhost joomla jsfiddle lastfm lastfm-square leanpub linkedin linkedin-square linode linux maxcdn meanpath medium meetup mixcloud modx odnoklassniki odnoklassniki-square opencart openid opera optin-monster pagelines paypal pied-piper pied-piper-alt pied-piper-pp pinterest pinterest-p pinterest-square product-hunt qq quora ra (alias) ravelry rebel reddit reddit-alien reddit-square renren resistance (alias) safari scribd sellsy share-alt share-alt-square shirtsinbulk simplybuilt skyatlas skype slack slideshare snapchat snapchat-ghost snapchat-square soundcloud spotify stack-exchange stack-overflow steam steam-square stumbleupon stumbleupon-circle superpowers telegram tencent-weibo themeisle trello tripadvisor tumblr tumblr-square twitch twitter twitter-square usb viacoin viadeo viadeo-square vimeo vimeo-square vine vk wechat (alias) weibo weixin whatsapp wikipedia-w windows wordpress wpbeginner wpexplorer wpforms xing xing-square y-combinator y-combinator-square (alias) yahoo yc (alias) yc-square (alias) yelp yoast youtube youtube-play youtube-square].to_set.freeze

  NEW_PREFIXES = %w[fab far fas].to_set.freeze

  # Takes in a line, looks for icon helper, replaces them if found
  # Next, looks for icon classes, replaces them if found
  # Returns the updated line
  def replace(line)
    updated_helper_line = line.gsub(ICON_HELPER_REGEX) do |match|
      new_icon = getNewIcon($~[:icon_name])

      if new_icon
        "'#{new_icon[:prefix]}', '#{new_icon[:new_class]}'"
      else
        match
      end
    end

    updated_class_line = updated_helper_line.gsub(ICON_CLASS_REGEX) do |match|
      new_icon = getNewIcon($~[:icon_name])

      if new_icon
        "#{new_icon[:prefix]} fa-#{new_icon[:new_class]}"
      else
        match
      end
    end

    if updated_class_line != line
      return updated_class_line
    else
      return nil
    end
  end

  def getNewIcon(old_icon_name)
    unless NEW_PREFIXES.include?(old_icon_name)
      new_icon = ICONS[old_icon_name]
      if new_icon.nil?
        new_icon =
          if BRANDS.include?(old_icon_name)
            { new_class: old_icon_name, prefix: 'fab' }
          elsif old_icon_name.end_with?("-o")
            { new_class: old_icon_name.sub("-o", ""), prefix: 'far' }
          else
            { new_class: old_icon_name, prefix: 'fas' }
          end
      end
      return new_icon
    end
  end

end

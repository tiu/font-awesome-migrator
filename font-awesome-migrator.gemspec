
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "font-awesome-migrator/version"

Gem::Specification.new do |spec|
  spec.name          = "font-awesome-migrator"
  spec.version       = FontAwesomeMigrator::VERSION
  spec.authors       = ["Colby Guyer"]
  spec.email         = ["cguyer@tiu11.org"]

  spec.summary       = 'Migrates Font Awesome 4 Icons and Class names to Font Awesome 5'
  spec.description   = 'Provides a bin task to migrate Font Awesome 4 Icons and Class names to Font Awesome 5 classes.'
  spec.homepage      = 'https://bitbuckets.org/tiu/font-awesome-migrator'
  spec.license       = 'Copyright Tuscarora Intermediate Unit 11'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "'https://tiu11.org'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_dependency 'colorize', '~> 0.8'
end

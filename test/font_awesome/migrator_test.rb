require "test_helper"

class FontAwesomeMigrator::Test < Minitest::Test

  def test_that_it_has_a_version_number
    refute_nil FontAwesomeMigrator::VERSION
  end

  def test_4_to_5_icon_renames
    assert FontAwesomeMigrator.getNewIcon('address-book-o') == {:new_class=>"address-book", :prefix=>"far"}
  end

  def test_4_to_5_icon_brand_prefixes
    assert FontAwesomeMigrator.getNewIcon('amazon') == {:new_class=>"amazon", :prefix=>"fab"}
  end

  def test_4_to_5_icon_outlines
    assert FontAwesomeMigrator.getNewIcon('home-o') == {:new_class=>"home", :prefix=>"far"}
  end

  def test_4_to_5_icon_default
    assert FontAwesomeMigrator.getNewIcon('home') == {:new_class=>"home", :prefix=>"fas"}
  end
end
